/*
 * Thunderhead API test code
 *
 * To use this code set the following environment variables:
 *   ONE_KEY    - API Key
 *   ONE_SECRET - API Secret
 *   ONE_USER   - API User
 *   ONE_SK_DEV - Site key for development space on ONE
 */

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var request = require('request');
var OAuth = require('oauth-1.0a');
var crypto = require('crypto');

function hash_function_sha1(base_string, key) {
    return crypto.createHmac('sha1', key).update(base_string).digest('base64');
}

var oauth = OAuth({
    consumer: {
        key: process.env.ONE_KEY + '!' + process.env.ONE_USER,
        secret: process.env.ONE_SECRET
    },
    signature_method: 'HMAC-SHA1',
    hash_function: hash_function_sha1
});

function post_interaction(requestData){
    var request_data = {
        url: 'https://eu2.thunderhead.com/one/oauth1/rt/api/2.0/interaction?sk=' + process.env.ONE_SK_DEV,
        method: 'POST'
    };
    console.log(requestData);
    request({
        url: request_data.url,
        method: request_data.method,
        headers: oauth.toHeader(oauth.authorize(request_data)),
        json: requestData,
    }, function(error, response, body){
        //process data here
        console.log(body);
    })
}

function get_structure(structure, ck){
    var request_data = {
        url: 'https://eu2.thunderhead.com/one/oauth1/rt/api/2.0/profiles/' + structure + '?sk=' + process.env.ONE_SK_DEV + '&ck=' +ck,
        method: 'GET'
    };

    var headers = oauth.toHeader(oauth.authorize(request_data));
    headers.Accept = 'application/json';
    //console.log(headers);
    console.log("Requesting structure '" + structure + "' for customer key " + ck);
    request({
        url: request_data.url,
        method: request_data.method,
        headers: headers
    }, function(error, response, body){
        //process data here
        console.log(body);
    })
}

// from here on is just example calls

var requestData1 = {
    "uri": "oehdyn://Test/consideration", // interaction URI
    "properties": [                           // properties which will be sent to ONE
        {
            "name": "customerKey",
            "value": "166F0DD6-3D7F-E611-80ED-5065F38B4681"
        },
        {
            "name": "propositionCode",
            "value": "membership_secondary"
        }
    ]
};

var requestData2 = {
    "uri": "oehdyn://Test/selection", // interaction URI
    "properties": [                           // properties which will be sent to ONE
        {
            "name": "customerKey",
            "value": "166F0DD6-3D7F-E611-80ED-5065F38B4681"
        },
        {
            "name": "propositionCode",
            "value": "membership_secondary"
        }
    ]
};

var requestData3 = {
    "uri": "oehdyn://Transactional/opportunity/opened", // interaction URI
    "properties": [                           // properties which will be sent to ONE
        {
            "name": "customerKey",
            "value": "166F0DD6-3D7F-E611-80ED-5065F38B4681"
        },
        {
            "name": "propositionCode",
            "value": "membership_secondary"
        }
    ]
};

post_interaction(requestData1);
post_interaction(requestData2);
post_interaction(requestData3);
get_structure('journeys','166F0DD6-3D7F-E611-80ED-5065F38B4681');
